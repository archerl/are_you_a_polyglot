const express = require('express');
const path = require('path');
const hbs = require('hbs');

// Take any port
const port = process.env.PORT || 3000;


const app = express();
const publicDirectortPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');


app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);


app.use(express.static(publicDirectortPath));


app.get('/', (req, res) => {
    if (req.headers["user-agent"].includes('MS-DOS')){
        res.render('flag')
    }else{
        res.render('index');
    }
});


app.listen(port, () => {
    console.log(`Server running at port ${port}`)
})