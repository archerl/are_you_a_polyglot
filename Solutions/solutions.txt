Open the browser, you will need a proxy software for this either Burp Suite or Zap.

Steps:
1. Before the website loads intercept the request, in the User-Agent header, remove your OS name and add "MS-DOS" (all caps).
2. You will now be redirected to a new page, with Link to the PDF.

Part-2:
This is a polyglot file, chances of it being flagged by a anti-virus is low, but in the events, special permissions might be required.

Once the file is downloaded, change the estension of the file from .pdf to .zip.
	- In linux this should be easy.
	- For windows [IN case not enabled] go to view in the navigation bar of the explorer window -> options -> Change folder and 
	search options. Navigate to the view tab in the opened dialog box, uncheck "Hide extensions for known file types" 

1. Once the file extension is changed, extract/open the "Open Me.txt" file, extract the flag.