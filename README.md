## Objective:
This web-app is expecting someone as the user, but can the user impersonate that someone? The goal is to provide the player with basic knowledge of how the website identifies the user based on certain properties that their browser sends to the server.


The web-app sets the basic usage and threat polyglots propose, they are not only useful in popping XSSs but also in malware engineering.


In the real world scenario, this is how the server determines which operating system is the user using. For example, if you download FireFox browser from their official website, it will automatically determine the operating system you are using. 

## Implementation
The app would/was built on basic nodemodules and express-js. The initial sourced code can be found [here](https://gitlab.com/archerl/are_you_a_polyglot) and a live demo instance of the website [here](https://are-you-a-polyglot-1.herokuapp.com/).

A dockerfile was also made to aide the deployment. Run the dockerfile with commands

1. `docker build -t <your username>/progressive-ctf-1 .`

2. `docker run -p anyport_out:3000 -d <your username>/progressive-ctf-1`


To check if it ran successfully or not.

`docker ps`

Open your browser and go to the `localhost:anyport_out`, to be able to run it with burp use `localhost.:anyport_out`

## features required
Before deploying, it is recommended to have a rate-throttling or rate-limiting in place on the endpoint.

## Constraints
The machine/web-app is very easy and its better to used under a time boundation for any CTF's purposes.

## Problem Statement

Problem: The website was built by Military police, like we all know they are lazy and incompetent, the website they built is very new and 
lacks security. Since you are Levi, you have to figure out a way to download the PDF file from the webpage.

The PDF file, has the instructions for the Flag, or is it the flag itself? But before that, you will have to download the PDF file from 
the website.

Hints:
PART:1
1. How do you think Firefox determines which version of the software to give you, like firefox for linux or firefox for windows.
PART:2 
2. What are polyglots?
3. Do you browse YouTube and are you a LiveOverflow's fan?
4. Watch the video on YouTube, titled "Is this is Dog" by LiverOverflow.
